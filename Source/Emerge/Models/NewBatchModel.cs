﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using Emerge.Services;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Emerge.Models
{
    public class NewBatchModel
    {
        public List<Proc_Get_ReportCategoriesResult> ReportCategoriesList { get; set; }
        public List<Proc_Get_EmergeProductPackagesResult> EmergeProductPackagesList { get; set; }
        public Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>> DictCategory { get; set; }
        public List<Proc_GetDrugTestingProductsResult> DrugTestingList { get; set; }
        public List<clsColumnList> ColumnList { get; set; }
        public List<SelectListItem> PermissiblePurpose { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public string Social { get; set; }

        public string DOB { get; set; }
        public string Phone { get; set; }
        public string ApplicantEmail { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string Apt { get; set; }
        public string City { get; set; }
        public string hfReports { get; set; }
        public string IsLiveRunner { get; set; }
        public string Zip { get; set; }
        public string Suffix { get; set; }     
        public string Direction { get; set; }
        public string Type { get; set; }
        public string State { get; set; }
        public string Jurisdiction { get; set; }
        public string JurisdictionText { get; set; }

        public string CountyInfoState { get; set; }
        public string CountyInfoCounty { get; set; }
        
        public string DLState { get; set; }
        public string VehicleVin { get; set; }
        public string DriverLicense { get; set; }
        public string Sex { get; set; }

        public string CompanyState { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyPhone { get; set; }
        public string AliasName { get; set; }

        public string SchoolCity { get; set; }
        public string SchoolPhone { get; set; }
        public string InstituteAlias { get; set; }
        public string SchoolName { get; set; }
        public string InstituteState { get; set; }


        public string BusinessState { get; set; }
        public string BusinessName { get; set; }
        public string BusinessCity { get; set; }

        public string ReferenceName { get; set; }
        public string ReferencePhone { get; set; }
        public string ReferenceAlias { get; set; }

        public string TrackingRefCode { get; set; }
        public string TrackingNotes { get; set; }
        public string ProductsSelected { get; set; }

        public string LicenseState { get; set; }
        public string LicenseType { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseIssueDate { get; set; }
        public string LicenseAlias { get; set; }


        public string hdnIsSCRLiveRunnerLocation { get; set; }

        public bool chkFCRA { get; set; }
        public bool chkIsNullDOB { get; set; }
        public bool chkIsBatchHeaders { get; set; }

        public int fkLocationId { get; set; }
        public int pkCompanyId { get; set; }
        public string PermissiblePurposeValue { get; set; }
        public int PermissiblePurposeId { get; set; }
        public string OrderCompanyName { get; set; }
        public string CompanyAccountNo { get; set; }
        public int pkCompanyUserId { get; set; }
        public int pkOrderDetailId { get; set; }

        public byte OrderType { get; set; }

        public string lblFrom { get; set; }
        public string lblTo { get; set; }
        public string Suggestions { get; set; }
        public string SheetName { get; set; }

        [Required]
        public string Stopper { get; set; }

        public string ControlList { get; set; }
        public string FileColumns { get; set; }
        public bool IsWCVAcknowledged { get; set; }

        public NewBatchModel()
        {
            chkFCRA = true;
            Stopper = string.Empty;
            ControlList = string.Empty;
            FileColumns = string.Empty;
            SheetName = string.Empty;
            chkIsNullDOB = false;
            OrderType = 1;
            ReportCategoriesList = new List<Proc_Get_ReportCategoriesResult>();
            EmergeProductPackagesList = new List<Proc_Get_EmergeProductPackagesResult>();
            DictCategory = new Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>>();
            PermissiblePurpose = new List<SelectListItem>();
        }
    }

    public class clsColumnList
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }

    public class TempBatch
    {
        public string BatchRequiredFields { get; set; }
        public string CollaspeId { get; set; }
        public string RegExp { get; set; }
        public bool? allowEmpty { get; set; }
    }
}