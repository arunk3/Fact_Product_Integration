﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using Emerge.Services;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;



using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
namespace Emerge.Models
{
    public class EmailSystemTemplatesModel
    {
        public int pkTemplateId { get; set; }
        public int TemplateCode { get; set; }

        [Required(ErrorMessage = "Please select Template Type")]
        public int TemplateType { get; set; }

        public Guid fkApplicationId { get; set; }


        [Required(ErrorMessage = "Please enter Template Name")]
        public string TemplateName { get; set; }

        [Required(ErrorMessage = "Please select Template Name")]
        public string ddlTemplateName { get; set; }

        [Required(ErrorMessage = "Please select Template")]
        public string ddlTemplateSubject { get; set; }


        public List<SelectListItem> ddlTemplateType { get; set; }

        [Required(ErrorMessage = "Please enter Template Subject")]
        public string TemplateSubject { get; set; }

        // [Required(ErrorMessage = "Please enter Template Content")]
        [AllowHtml]
        public string TemplateContent { get; set; }

        public string DefaultTemplateContent { get; set; }

        public bool IsSystemTemplate { get; set; }
        public bool IsNotificationTemplate { get; set; }
        public bool IsEnabled { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }



        public List<Proc_USA_LoadEmailTemplatesResult> EmailTemplatesList { get; set; }

        public List<Proc_USA_LoadEmailTemplatesResult> SystemTemplatesList { get; set; }

        #region For Newsletter

        [Required(ErrorMessage = "Please select Company Name")]
        public int pkCompanyId { get; set; }


        public string CompanyName { get; set; }

        public int UserType { get; set; }

        public List<CompanyList> lstCompanyList { get; set; }

        public Dictionary<int, string> DicUserType { get; set; }

        #endregion For Newsletter
    }

    public class MangoChatSessionModel
    {


        public List<Proc_Get_ChatInfoBySessionIdForUserResult> GetChatHistorybySessionId { get; set; }
        public string SuccessMsg { get; set; }
    }

    public class Product : Proc_Get_AllChatHistoryResult
    {

        public string Id { get; set; }
        public string ClientId { get; set; }
        public string Channel { get; set; }
        public string PublisherId { get; set; }
        public DateTime? Time { get; set; }
        public string Timestamp { get; set; }
        public string Service { get; set; }
        public string Tag { get; set; }
        public string ExtraInfo { get; set; }
        public string ChatSession { get; set; }
        public string FromUserId { get; set; }
        public string ToUserId { get; set; }
        public string ActualContent { get; set; }
    }
}