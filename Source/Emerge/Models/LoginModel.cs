﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;


namespace Emerge.Models
{
    public class LoginModel
    {
        
        public LoginModel()
        {
            listDefaultPages = new List<SelectListItem>();
        }

        [Required(ErrorMessage = "Please Enter Username")]
        public string UserName { get; set; }
        public string UserImage { get; set; }
        public string UserFistLastName { get; set; }
        public string CompanyName { get; set; }
       
        [Required(ErrorMessage = "Please Enter Password")]
       
        public string Password { get; set; }
        public string DefaultPage { get; set; }
        public bool RemeberMe { get; set; }
        public List<SelectListItem> listDefaultPages { get; set; }
        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Enter Valid Email Id.")]
        public string Email { get; set; }
        public bool IsOpenFeature { get; set; }
        public int? FeatureId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UploadedFile { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsPrevious { get; set; }
        public bool IsNext { get; set; }
        public int CurrentFeature { get; set; }
        public int Featurecount { get; set; }
        public Proc_GetFeaturesForUserResult NewFeature = new Proc_GetFeaturesForUserResult();
        public string Image { get; set; }
        public string TitleImage { get; set; }
        public List<MyFormModelColl> Collection { get; set; }
        public int TotalCounts { get; set; }
        public int RowCounts { get; set; }

        [Required(ErrorMessage = "Please enter Old Password")]
        [DataType(DataType.Password)]
        [DisplayName("Old Password")]        
        public string OldPassword { get; set; }
        
        [Required(ErrorMessage = "Please enter New Password")]
        [DataType(DataType.Password)]
        [DisplayName("New Password")]
       // [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(?=.*?[0-9].*?[0-9])(?=.*[!@#$%])[0-9a-zA-Z!@#$%0-9]{6,}$", ErrorMessage = "Password must meet complexity requirements.")]
        public string NewPassword { get; set; }
        
        [DisplayName("Confirm New Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter Confirm New Password")]
        [Compare("NewPassword", ErrorMessage = "Passwords must match")]
       // [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(?=.*?[0-9].*?[0-9])(?=.*[!@#$%])[0-9a-zA-Z!@#$%0-9]{6,}$", ErrorMessage = "Password must meet complexity requirements.")]
        public string ConfirmNewPassword { get; set; }


    }
}