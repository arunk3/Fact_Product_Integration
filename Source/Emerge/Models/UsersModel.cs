﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Services;
using System.ComponentModel.DataAnnotations;
namespace Emerge.Models
{
    public class UsersModel
    {
        #region tblCompanyUser

        public int pkCompanyUserId { get; set; }
      
        public Guid fkUserId { get; set; }
        public int? fkLocationId { get; set; }

        public string UserNote { get; set; }

        public string UserCode { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserAddressLine1 { get; set; }

        public string UserAddressLine2 { get; set; }

        public string PhoneNo { get; set; }
        public string TaleoApiKey { get; set; }// nd-16
        public string ZipCode { get; set; }

        public string UserImage { get; set; }

        public int ?fkStateId { get; set; }

        public int ?fkCountyId { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsSignedFcra { get; set; }
        public bool IsLiveRunnerEnabled { get; set; }
        public DateTime CreatedDate { get; set; }

        public string Initials { get; set; }
        #endregion tblCompanyUser

        public int PkCompanyId { get; set; }
        public int CurrentUserLocationId { get; set; }
        public string CurrentUserRole { get; set; }
        public string CurrentUserEmail { get; set; }

        public string PageTitle { get; set; }

        public string SelectedCompanyName { get; set; }

       
        public string LastLoginByUser { get; set; }

        public string UserEmail { get; set; }
        public string UserEmailOne { get; set; }
        public string UserEmailTwo { get; set; }

        public string OldUserEmail { get; set; }

        public string UserPassword { get; set; }
        public string OldUserPassword { get; set; }

        public string RetypeUserPassword { get; set; }

        public string OldUserImage { get; set; }
        public string ViewImage { get; set; }

        public string QueryStringUserId { get; set; }
        public string QueryStringCompanyId { get; set; }

        public string CurrentEditUserRoleName { get; set; }
        public string CurrentLoginUserRoleName { get; set; }

        public string ddlCompanyEnabled { get; set; }
        public string ddlLocationEnabled { get; set; }
        public string ddlStatusEnabled { get; set; }
        public string txtPasswordMode { get; set; }
        public string txtPasswordReadOnly { get; set; }

        //Used for autoreview on user level.
        public bool IsAutoEmergeReview { get; set; }//INT202

        public bool IsAdmin { get; set; }
        public bool IsRootCompany { get; set; }
        public Guid RoleId { get; set; }      
        
        public string Description { get; set; }

        public int CompanyUserId { get; set; }

        public string strResult { get; set; }

        public Guid ManagerUserId { get; set; }
        public string ManagerFullName { get; set; }

        public bool IsPageModeAdd { get; set; }
        public bool IsPageModeEdit { get; set; }
        public bool IsPageModeView { get; set; }

        public bool trManagerVisible { get; set; }
        public string ddlManagersEnabled { get; set; }
        public int Selected_RelationID { get; set; }
        public string Selected_RoleName { get; set; }
        public bool IsEdit { get; set; }

        public string OtherLocationId { get; set; }
        public string SelectedLocationId { get; set; }


        public List<tblState> CollStates = new List<tblState>();

        public List<aspnet_Role> CollRoles = new List<aspnet_Role>();

        public List<tblLocation> CollLocations = new List<tblLocation>();


        public List<clsCounty> CollCounties = new List<clsCounty>();

        public List<CompanyList> CollCompany = new List<CompanyList>();
        public List<tblLocation> objtblLocation = new List<tblLocation>();
        
        public List<Proc_Get_UsersUnderCompanyResult> CollUsersUnderCompany = new List<Proc_Get_UsersUnderCompanyResult>();
        public List<Proc_Get_UsersUnderLocationResult> CollUsersUnderLocation = new List<Proc_Get_UsersUnderLocationResult>();
        
        public List<SelectListItem> ListCompany_Id { get; set; }
    }
}