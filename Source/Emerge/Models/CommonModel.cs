﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class CommonModel
    {
    }

    public class GridSort
    {
        public string Field { get; set; }
        public string Dir { get; set; }
    }

    public class GridFilter
    {
        public string Field { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
    }

    public class CallLogModel
    {
        public List<Proc_Get_CommentsForLeadResult> CallLogCommentList { get; set; }
        public List<SelectListItem> CompanyList { get; set; }
        public string CallLogCompany {get; set;}
        public string   PageType { get; set; }
        public bool IsCallLogOpen { get; set; }
        public bool IsCallLogIcon { get; set; }
        public int ComapnyStatus { get; set; }
        public List<SelectListItem>  ddlCompanyUsers { get; set; }
        public string SelectedComapnyName { get; set; }
        [AllowHtml]
        public string CallLogHeader { get; set; }

        public CallLogModel()
        {
            CallLogCommentList=new List<Proc_Get_CommentsForLeadResult>();
            CompanyList = new List<SelectListItem>();
        }
    }
    public class AdminSearchModel
    {
        public string strResultNewAccounts { get; set; }
        public string strResultAdminConsent { get; set; }

        public int DrugTestingReportType { get; set; }
        public string lnkDrugTesting { get; set; }

        public int HelpWithOrdersReportType { get; set; }
        public string lnkHelpWithOrders { get; set; }


        public int EmergeReviewReportType { get; set; }
        public string lnkEmergeReview { get; set; }


        public int AdminConsentReportType { get; set; }
        public string lnkAdminConsent { get; set; }

        public bool SystemNotificationStatus { get; set; }
        public string SystemNotificationTitle { get; set; }
        public string SystemNotificationContent { get; set; }
        public List<Proc_USA_LoadEmailTemplatesResult> CollTemplateContents = new List<Proc_USA_LoadEmailTemplatesResult>();

        public List<proc_GetConsentInforByOrderIdResult> CollGetConsentInforByOrderId = new List<proc_GetConsentInforByOrderIdResult>();
        
        [Required(ErrorMessage = "Please select file")]
        public IEnumerable<HttpPostedFileBase> ConsentFileUpload { get; set; }
   
        public string fileUpload { get; set; }
      
    }

    public class NewFeatureOpenModel
    {
        public bool IsOpenFeature{ get; set; }
        public int ? FeatureId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UploadedFile { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsPrevious { get; set; }
        public bool IsNext { get; set; }
        public int CurrentFeature { get; set; }
        public int Featurecount { get; set; }
        public Proc_GetFeaturesForUserResult NewFeature = new Proc_GetFeaturesForUserResult();
        public string Image { get; set; }
        public string TitleImage { get; set; }

    }


    public class OfficeModel
    {
        public int DemoScheduleId { get; set; }
        int CompanyId { get; set; }
        public List<Proc_GetTraineeBy_DemoId_CompanyIdResult> CollGetTraineeByDemoIdCompanyId = new List<Proc_GetTraineeBy_DemoId_CompanyIdResult>();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool status { get; set; }

    }


 public class SystemNotificationModel
    {

        public bool SystemNotificationStatus { get; set; }
        public string SystemNotificationTitle { get; set; }

        [AllowHtml]
        public string SystemNotificationContent { get; set; }

        public List<Proc_USA_LoadEmailTemplatesResult> CollTemplateContents = new List<Proc_USA_LoadEmailTemplatesResult>();


    }









}