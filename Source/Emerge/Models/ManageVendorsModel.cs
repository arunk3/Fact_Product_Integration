﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using Emerge.Services;
using System.Xml;

namespace Emerge.Models
{
    public class ManageVendorsModel
    {
        public byte pkVendorId { get; set; }
        public string VendorName { get; set; }
        public string ServiceUrl { get; set; }
        public string ServiceUserId { get; set; }
        public string ServicePassword { get; set; }
    }

    public class ManageProductVendorPriceModel
    {
        public int pkProductApplicationId { get; set; }
        public bool? IsGlobal { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDefault { get; set; }
        public string ProductName { get; set; }
        public string ProductDisplayName { get; set; }
        public string ProductCode { get; set; }
        public decimal ProductVendorPrice { get; set; }
        public decimal ProductPerApplicationPrice { get; set; }
        
    }
}