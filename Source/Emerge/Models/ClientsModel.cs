﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


namespace Emerge.Models
{
    public class ClientsModel
    {
        public string CompanyType { get; set; }
        public byte pkCompanyTypeId { get; set; }
        public int pkCompanyId { get; set; }
        public string statustype { get; set; }
        public Guid? fkUserId { get; set; }
        public Guid? fkSalesAssociateId { get; set; }
        public string FullName { get; set; }
        public string Accountdate { get; set; }
        public string Keyword { get; set; }
        public int Recordperpage { get; set; }
        public bool isnullRecord { get; set; }
        public string txtSpecificDate2 { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string txtSpecificFrom { get; set; }
        public string txtSpecificTo { get; set; }
        public int pkStateId { get; set; }
        public string CityName { get; set; }
        public int fkStateId { get; set; }
        public string Priority2 { get; set; }
        public string Priority02 { get; set; }
        public string StateName { get; set; }
        public bool IsAdmin { get; set; }

        public string CompanyAccountNumber { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCod { get; set; }
        public string MainContact { get; set; }
        public string MainEmail { get; set; }
        public string MainPhone { get; set; }
        public string MainFaxNumber { get; set; }
        public string BillingContact { get; set; }
        public string BillingEmail { get; set; }
        public string BillingPhone { get; set; }
        public byte fkCompanyTypeId { get; set; }
        public string CompanyNote { get; set; }
        public string Review { get; set; }
        public DateTime MemberSince { get; set; }
        public string FirstReview { get; set; }
        public string LastReview { get; set; }
        public bool IsEdit { get; set; }
        public string FaxNumber2 { get; set; }
        public string Rating { get; set; }
        public string txtOpenSearch { get; set; }
        public List<tblCompanyDocument> CompnayDocList { get; set; }
        public int pkPermissibleId { get; set; }
        public List<SelectListItem> PermissiblePurposeList { get; set; }
        public List<Proc_Get_CommentsForLeadResult> CollGetCommentsForLeadResult = new List<Proc_Get_CommentsForLeadResult>();
        public List<Proc_Get_Recent_ReviewClientsResult> RecentReviewList = new List<Proc_Get_Recent_ReviewClientsResult>();
        public List<proc_GetSalesValuesforGraphMVCResult> listofGraph = new List<proc_GetSalesValuesforGraphMVCResult>();

        public List<tblCompanyType> listCompanyType = new List<tblCompanyType>();
        public List<Proc_Get_SalesManagersResult> listSalesManagersResult = new List<Proc_Get_SalesManagersResult>();
        public List<tblState> listStates = new List<tblState>();
        public List<Proc_Get_Recent_ReviewsResult> RecentReviewLeadList = new List<Proc_Get_Recent_ReviewsResult>();
        //public List<tblState> listStates = new List<tblState>(); 
        public ClientsModel()
        {
            PermissiblePurposeList = new List<SelectListItem>();
        }
    }
}