﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class DrugTestingProductModel
    {
        public int pkProductApplicationId { get; set; }
        public int pkProductId { get; set; }
        public string ProductDisplayName { get; set; }
        public decimal ProductPerApplicationPrice { get; set; }
        public string LongDescription { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsGlobal { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsNonMemberRpt { get; set; }
        public DateTime CreatedDate { get; set; }

        [Required(ErrorMessage = "Please select file")]
        public string ProductImage { get; set; }
        public string ProductImagepath { get; set; }
        public string strResult { get; set; }
        public byte? DrugTestCategoryId { get; set; }
        public string DrugTestCategoryName { get; set; }
        public string DrugTestQty { get; set; }
        public string DrugTestPanels { get; set; }
        public string ProductCode { get; set; }
        public byte? SortOrder { get; set; }
        public int fkReportCategoryId { get; set; }
        public decimal? SalivaPrice { get; set; }
        public decimal? EZcupPrice { get; set; }



    }
}