﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using Emerge.Services;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Emerge.Models
{
    public class NewReportModel
    {

        public List<Proc_Get_ReportCategoriesResult> ReportCategoriesList { get; set; }
        public List<Proc_Get_EmergeProductPackagesResult> EmergeProductPackagesList { get; set; }
        //public List<Proc_Get_NewReportsByCategoryIdResult> NewReportsByCategoryList { get; set; }
        public List<Proc_GetDrugTestingProductsResult> DrugTestingList { get; set; }
        public List<tblReferenceCode> ReferenceCodeList { get; set; }
        public List<clsStates> StateList { get; set; }
        public List<clsStates> StateListMVR { get; set; }
        public List<tblState> ObjStateList { get; set; }
        public List<clsCounty> ObjCountyList { get; set; }
        public List<clsJurisdiction> JurisdictionList { get; set; }


        public Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>> DictCategory { get; set; }
        public Dictionary<string, string> SuffixList { get; set; }
        public Dictionary<string, string> GenderList { get; set; }
        public Dictionary<string, string> DirectionList { get; set; }
        public Dictionary<string, string> TypeList { get; set; }
        public List<SelectListItem> PermissiblePurpose { get; set; }
        public List<SelectListItem> CompanyLocations { get; set; }

        public bool Is7YearFilter { get; set; }
        public bool Is7YearAuto { get; set; }
        //int-9
        public bool isfcrjurisdictionfee { get; set; }
        public bool Is7YearFilterApplyForAllCriminalResult { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }

        // [StringLength(11, MinimumLength = 11, ErrorMessage = "Enter valid No.")]
        public string Social { get; set; }
        public string SocialMask { get; set; }


        [Required(ErrorMessage = "*")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "*")]
        public string Password { get; set; }

        public string lblFrom { get; set; }
        public string lblTo { get; set; }

        public string DOB { get; set; }

        public string Phone { get; set; }
        public string ApplicantEmail { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string Apt { get; set; }
        public string City { get; set; }
        public string hfReports { get; set; }
        public string IsLiveRunner { get; set; }
        public string Zip { get; set; }
        public bool IsMIEnable { get; set; }
        public bool IsSyncpodforcompany { get; set; }
        public string SuffixValue { get; set; }
        public string Sex { get; set; }
        public string DirectionValue { get; set; }


        public string TypeValue { get; set; }
        public string VehicleVin { get; set; }
        public string DriverLicense { get; set; }
        public string hdnIsSyncpod { get; set; }
        public string hdnFaceImage { get; set; }
        public string hdnSyncpodImage { get; set; }
        public string hdnIsSCRLiveRunnerLocation { get; set; }

        //public List<CountryList> ObjCounryList { get; set; }

        public int pkCountyId { get; set; }
        public int pkStateId { get; set; }
        public string pkStateIdSelected { get; set; }
        public string LiveRunnerState { get; set; }
        public string DLState { get; set; }
        public int CompanyState { get; set; }
        public int InstituteState { get; set; }
        public int LicenseState { get; set; }
        public int BusinessState { get; set; }
        public int TrackingRefCode { get; set; }

        public int CountyInfoState { get; set; }
        public string CountyInfoCounty { get; set; }

        public string State { get; set; }
        public string Jurisdiction { get; set; }

        public string JurisdictionText { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyPhone { get; set; }
        public string AliasName { get; set; }

        public string SchoolCity { get; set; }
        public string SchoolPhone { get; set; }
        public string InstituteAlias { get; set; }
        public string SchoolName { get; set; }
        public string SearchedGraduationDt { get; set; }//INT336 GraduationDt

        public string ReferenceName { get; set; }
        public string ReferencePhone { get; set; }
        public string ReferenceAlias { get; set; }

        public string PReferenceName { get; set; }//INT336 Professional Reference.
        public string PReferencePhone { get; set; }//INT336 Professional Reference.
        public string PReferenceAlias { get; set; }//INT336 Professional Reference.

        public string LicenseType { get; set; }

        public string LicenseNumber { get; set; }
        public string LicenseIssueDate { get; set; }
        public string LicenseAlias { get; set; }

        public string BusinessName { get; set; }
        public string BusinessCity { get; set; }
        public string PermissiblePurposeValue { get; set; }
        public int PermissiblePurposeId { get; set; }
        public string OrderCompanyName { get; set; }
        public string CompanyAccountNo { get; set; }
        public string TrackingRefCodeValue { get; set; }


        public string TrackingNotes { get; set; }
        public string ProductsSelected { get; set; }
        public string TrackingRefCodeText { get; set; }

        public string Suggestions { get; set; }

        public bool chkFCRA { get; set; }
        public bool chkCharge { get; set; }
        public bool chkIsNullDOB { get; set; }
        public bool AllowJurisdiction { get; set; }
        public bool IsAuthenicated { get; set; }


        public int ParentOrderId { get; set; }
        public int fkLocationId { get; set; }
        public int pkCompanyId { get; set; }
        public int pkCompanyUserId { get; set; }
        public int pkOrderDetailId { get; set; }

        public bool SystemNotificationStatus { get; set; }

        [AllowHtml]
        public string SystemNotification { get; set; }

        public string ReportNotGeneratingProfile { get; set; }

        public List<Proc_USA_LoadEmailTemplatesResult> CollTemplateContents = new List<Proc_USA_LoadEmailTemplatesResult>();

        public string IsErrorLog { get; set; }


        public bool FCRAStatus { get; set; }
        public byte OrderType { get; set; }
        public int OrderId { get; set; }
        public List<tblOrderSearchedEducationInfo> ObjSearchedEducationInfo { get; set; }
        public List<tblOrderSearchedEmploymentInfo> ObjSearchedEmploymentInfo { get; set; }
        public List<tblOrderSearchedPersonalInfo> ObjOrderSearchedPersonalInfo { get; set; }
        public List<tblOrderSearchedLicenseInfo> ObjOrderSearchedLicenseInfo { get; set; }
        public List<Proc_Get_NewReportsForLandingPageResult> LandingReports { get; set; }
        public List<tblOrderSearchedLiveRunnerInfo> ObjOrderSearchedLiveRunnerInfo { get; set; }
        public string TopLeft { get; set; }
        public string TopRight { get; set; }
        public string ErrorLogHeading { get; set; }
        public string ErrorLog { get; set; }

        public string CompanyDisplayName { get; set; }
        public string CompanyLogo { get; set; }
        public string CompanyAddress { get; set; }
        public string ErrorMessage { get; set; }
        public bool RemeberMe { get; set; }
        public string UserFistLastName { get; set; }
        public string UserImage { get; set; }
        public string ApiUrl { get; set; }
        public string logincheck { get; set; }
        public bool IsWCVAcknowledged { get; set; }
        public string Urlpath { get; set; }
        public bool IsLiveRunnerEnabled { get; set; }
        public decimal? SalivaPrice { get; set; }
        public decimal? EZcupPrice { get; set; }
        public bool IsEscreenEnable { get; set; }
        public bool IsturnoffSSN { get; set; }
        public bool isEnabledBillingLocation { get; set; }
        public NewReportModel()
        {
            ErrorMessage = string.Empty;
            IsAuthenicated = true;
            OrderType = 1;
            AllowJurisdiction = false;
            IsLiveRunner = "false";
            chkIsNullDOB = false;
            ObjCountyList = new List<clsCounty>();
            DrugTestingList = new List<Proc_GetDrugTestingProductsResult>();
            ReferenceCodeList = new List<tblReferenceCode>();
            JurisdictionList = new List<clsJurisdiction>();
            StateListMVR = new List<clsStates>();
            ObjSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
            ObjSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
            ObjOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
            ObjOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();
            LandingReports = new List<Proc_Get_NewReportsForLandingPageResult>();
            ObjOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
            PermissiblePurpose = new List<SelectListItem>();
        }
    }


}