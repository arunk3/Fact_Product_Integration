﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class JunosModel
    {
        public string AllJunos { get; set; }
       public string NewProspects{ get; set; }
        public string AddedLeads{ get; set; }
       public string CallBack { get; set; }
       public string NotInterested { get; set; }
       public string BadLeads { get; set; }
       public string Message { get; set; }


    } 
}