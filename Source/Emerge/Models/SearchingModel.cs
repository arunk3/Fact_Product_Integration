﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emerge.Models
{
    public class SearchingModel
    {
        public string ImageUrl { get; set; }
        public string UserInfo { get; set; }
        public string Username { get; set; }
        public string IsAdditionalReport { get; set; }
    }
}