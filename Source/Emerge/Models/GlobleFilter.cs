﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class GlobleFilterModel
    {
        public int GlobleFilterId { get; set; }
        public string ReportType { get; set; }        
        public string Keywords { get; set; }        
        public string Source { get; set; }        
        public string DocketNumber { get; set; }
        public string DeliveryAddress { get; set; }
        public bool IsEnable { get; set; }
    }
}