﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Emerge.Models
{
    public class MyFormModel
    {
        public List<MyFormModelColl> Collection { get; set; }
        public List<CustomFormModelColl> CustomFormCollection { get; set; }
        public int TotalCounts { get; set; }
        public int RowCounts { get; set; }
        public string Message { get; set; }

        [Required(ErrorMessage = "Please select file")]
        public string PDFfileName { get; set; }
        [Required(ErrorMessage = "Please Enter Description")]
        public string CustomFormDescription { get; set; }
    }

    public class MyFormModelColl
    {
        public int pkformId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public Guid ModifiedLoggedId { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }

    public class CustomFormModelColl
    {
        public int PkCustomFormId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public Guid ModifiedLoggedId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid UpdatedByUserId { get; set; }

    }
}