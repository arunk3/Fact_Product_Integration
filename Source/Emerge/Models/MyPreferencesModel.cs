﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Emerge.Models
{
    public class MyPreferencesModel
    {
        public MyPreferencesModel()
        {
            listDefaultPages = new List<SelectListItem>();
            listSortColumn = new List<SelectListItem>();
            listRecordSize = new List<SelectListItem>();
        }
        public List<SelectListItem> listDefaultPages { get; set; }
        public string DefaultPage { get; set; }

        public List<SelectListItem> listSortColumn { get; set; }
        public string SortColumn { get; set; }

        public List<SelectListItem> listRecordSize { get; set; }
        public string RecordSize { get; set; }
        public bool IsDataEntryOnly { get; set; }

        public List<NotificationEmailAlertModel> lstNotificationEmailAlertModel { get; set; }
        public NotificationEmailAlertModel ObjNotificationEmailAlertModel { get; set; }
    }

    public class ReferenceCodeModel
    {
        public int pkReferenceCodeId { get; set; }
        public string ReferenceCode { get; set; }
        public string ReferenceNote { get; set; }
    }



    //Start INT-371 

    public class ManageFolderModel
    {
        public int pkManageFolderId { get; set; }
        public string FolderName { get; set; }
        public string FolderDescription { get; set; }
    }

    //END INT-371 


    public class NotificationEmailAlertModel
    {
        public int pkEmailAlertId { get; set; }
        public int fkTemplateId { get; set; }
        public int? fkCompanyUserId { get; set; }
        public string TemplateName { get; set; }
        public bool IsSendEmail { get; set; }
        public bool IsSendAlert { get; set; }
    }

}