﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using System.ComponentModel.DataAnnotations;


namespace Emerge.Models
{
    public class AdCompanyModel
    {       
        public int pkCompanyId { get; set; }
        public int fkCompanyTypeId { get; set; }
        public Guid fkApplicationId { get; set; }
        public Guid fkSalesRepId { get; set; }
        public string CompanyAccountNumber { get; set; }
        public string Companycode { get; set; }
        public string CompanyNote { get; set; }
        public string CompanyName { get; set; }
        public string FranchiseName { get; set; }
        public string MainContactPersonName { get; set; }
        public int Amount { get; set; }
        public string BillingContactPersonName { get; set; }
        public int tel { get; set; }
        public string BillingEmailId { get; set; }
        public string MainEmailId { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public int pkReferenceCodeId {get;set;}
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string CompanyType { get; set; }
        public Guid CreatedById { get; set; }
        public Guid LastModifiedById { get; set; }
        public bool IsEnableReview { get; set; }
        public string ReviewComments { get; set; }       
        public int pkProductApplicationId { get; set; }
        public int CompanyPrice{get;set;}
        public int fkCompanyId { get; set; }
        public int pkProductId { get; set; }
        public int ObjCompLocationId { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDefaultPrice { get; set; }
        public string FirstReview { get; set; }
        public DateTime MemberSince { get; set; }
        public string LastReview { get; set; }
        public string Rating { get; set; }
        public string Review { get; set; }
        public string ComapnyNote { get; set; }

        public string graphvalue { get; set; }

        public int pkInvoiceId { get; set; }
        public int InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime InvoiceDueDate { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal InvoiceAdjustAmount { get; set; }
        public decimal RemainingBalance { get; set; }

        public decimal ? TotalSale  { get; set; }
        public decimal ? ReportSale { get; set; }
        public decimal ? Productsale { get; set; }

        public string InvoiceErrormsg { get; set; }



      
       
        public decimal ProductPerApplicationPrice { get;set; }

        public bool IsPDFShow { get; set; }

        public string DocumentsTitle { get; set; }

        public bool isnull { get; set; }
       
         public byte pkCompanyTypeId { get; set; }
         public bool edit { get; set; }

       
        
       
              
        
        public string ReferenceCode{get; set; }
        public string ReferenceNote { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
       
       

        
        public string Ext1 { get; set; }
        public string Ext2 { get; set; }

        public bool IsReportEmailActivated { get; set; }

        //public string Email { get; set; }
        public bool SYNCPOD { get; set; }

        public List<proc_GetSalesValuesforGraphResult> GraphValue { get; set; }
        public string MonthYear { get; set; }

        public int value { get; set; }
        //public List<proc_LoadOpenInvoiceByCompanyIdResult> OpenInvoiceLoad { get; set; }

        public string FullName { get; set; }
        public Guid? fkUserId { get; set; }
        public Guid? fkSalesAssociateId { get; set; }

        public string Fname { get; set; }
        public string StateName { get; set; }
        public int pkStateId { get; set; }
        public string CityName { get; set; }
        public int? pkCityId { get; set; }

        public int State { get; set; }
        public string City { get; set; }
        public string ZipCod { get; set; }
        public string MainContact { get; set; }
        public string MainEmail { get; set; }
        public string MainPhone { get; set; }
        public string MainFaxNumber { get; set; }

        public string BillingContact { get; set; }
        public string BillingEmail { get; set; }
        public string BillingPhone { get; set; }
        public string BillingFax { get; set; }
        public byte PaymentTerm { get; set; }
        public string ReportEmailNotification { get; set; }

    }

    public class AdCompanyModel1
    {
        public int pkCompanyId { get; set; }
        //[Required(ErrorMessage = "Please Enter Address ")]
        public string Address1 { get; set; }
         //[Required(ErrorMessage = "Please Enter Address")]
        public string Address2 { get; set; }
        public bool IsEdit { get; set; }
        public string FirstReview { get; set; }
        public DateTime MemberSince { get; set; }
        public string LastReview { get; set; }
        public string Rating { get; set; }
        public string Review { get; set; }
         public string CompanyNote { get; set; }
         //[Required(ErrorMessage = "Enable Company")]
        public bool IsEnabled { get; set; }
         public List<tblCompanyDocument> CompnayDocList { get; set; }
       //[Required(ErrorMessage = "Please Enter Company Name")]
        public string CompanyName { get; set; }
        public string CompanyType { get; set; }
        //[Required(ErrorMessage = "Please Select Company Type")]
        public byte pkCompanyTypeId { get; set; }
        public int ObjCompLocationId { get; set; }
        public string CompanyAccountNumber { get; set; }
        public string DocumentsTitle { get; set; }
        public string FullName { get; set; }
        //[Required(ErrorMessage = "Please Select Account Manager")]
        public Guid? fkUserId { get; set; }
        public Guid? fkSalesAssociateId { get; set; }
        public string Fname { get; set; }
        public string StateName { get; set; }
        //[Required(ErrorMessage = "Please Select  State")]
        public int pkStateId { get; set; }
        public string CityName { get; set; }
        public int pkCityId { get; set; }
        public int State { get; set; }
        //[Required(ErrorMessage = "Please Select  city")]
        public string City { get; set; }
        //[Required(ErrorMessage = "Please Enter Zip Code")]
        //[RegularExpression((@"^[0-9]*$"), ErrorMessage = "Use Numeric Values")]
        public string ZipCod { get; set; }
       // [Required(ErrorMessage = "Please Enter Main Contact Name")]
        //[RegularExpression((@"^[A-Za-z_!@#$%^&*()-+= ]*$"), ErrorMessage = "Use Letters Only ")]
        public string MainContact { get; set; }
        //[Required(ErrorMessage = "Please Enter Main Email ")]
        //[RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Enter Valid Email Id.")]
        public string MainEmail { get; set; }
         //[RegularExpression(@"^(\d{10})$", ErrorMessage = "Please enter proper phone number.")]
         //[DataType(DataType.PhoneNumber)]
         //[Required(ErrorMessage = "Please Enter Phone")]
         public string MainPhone { get; set; }
        //[Required(ErrorMessage = "Please enter man fax number")]
        public string MainFaxNumber { get; set; }
        //[Required(ErrorMessage = "Please Enter Billing Contact Name")]
        public string BillingContact { get; set; }
        //[Required(ErrorMessage = "Please Enter Email")]
        //[RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Enter Valid Email Id.")]
        public string BillingEmail { get; set; }
        //[Required(ErrorMessage = "Please enter fax mobile number")]
        //[RegularExpression(@"^(\d{10})$", ErrorMessage = "Please enter proper phone number.")]
        public string BillingPhone { get; set; }
        //[Required(ErrorMessage = "Please enter fax number for billing")]
        public string BillingFax { get; set; }
        public byte PaymentTerm { get; set; }
        public int fkCompanyTypeId { get; set; }
        public bool IsReportEmailActivated { get; set; }
        //[Required(ErrorMessage = "Please enter email")]
        //[RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Enter Valid Email Id.")]
        public string ReportEmailNotification { get; set; }
        public bool IsPDFShow { get; set; }
        public string ReviewComments { get; set; }
        public bool IsHome { get; set; }
        public string Message { get; set; }
        public bool IsEnableAPI { get; set; }
        public bool IsIntegration { get; set; }
        public string APIUsername { get; set; }
        public string APIPassword { get; set; }
        public string APIPaymentTerms { get; set; }
        public string CompanyLogo { get; set; }
        public int? APILocation { get; set; }
        public string SelectedLocationId { get; set; } 
        public bool IsGlobalCredential { get; set; }
        public List<SelectListItem> APILocationList { get; set; }
        public int pkPermissibleId { get; set; }
        public List<SelectListItem> PermissiblePurposeList { get; set; }
        public bool IsReportStatus { get; set; }
        public bool IsturnoffSSN { get; set; }
        public bool isEnabledBillingLocation { get; set; }
        public AdCompanyModel1()
        {
            APILocationList = new List<SelectListItem>();
            PermissiblePurposeList = new List<SelectListItem>();
        }

        public string hdnPSProdId { get; set; }
        public string hdnNCRProdId { get; set; }
        public string hdnCCRProdId { get; set; }
        public string hdnEEIProdId { get; set; }
        public string hdnEMVProdId { get; set; }

        public List<Proc_Get_CommentsForLeadResult> CollGetCommentsForLeadResult = new List<Proc_Get_CommentsForLeadResult>();

        public string ApIUrl { get; set; }
        public string SecondaryPhone { get; set; }
        public string SecondaryFax { get; set; }

        public string State_Code { get; set; }
        public string TxtCallLog { get; set; }
        public bool Is7YearFilter { get; set; }
        public bool Is7YearAuto { get; set; }
        //int-9
        public bool isfcrjurisdictionfee { get; set; }
        //INT-219 define SSN alert parameter for a comapny.
        public bool IsDuplicateSSNReportAlertEnabled { get; set; }
    }

    public partial class SubReportsModel
    {

        public int pkProductId{get;set;}
        public int pkProductApplicationId{get;set;}
        public string ProductCode{get;set;}
        public string ProductName{get;set;}
        public string ProductDisplayName{get;set;}
        public string CategoryName{get;set;}
        public bool IsThirdPartyFee{get;set;}
        public byte ReportCategoryId{get;set;}
        public System.Nullable<byte> SortOrder{get;set;}
        public bool IsLiveRunnerProduct{get;set;}
        public System.Nullable<bool> IsLiveRunnerLocation{get;set;}
        //public System.Nullable<byte> AutoReviewStatusForLocation{get;set;}
        public int? ProductAccessId{get;set;}
        public System.Nullable<bool> IsDefault{get;set;}
        public System.Nullable<bool> IsGlobal{get;set;}
        public decimal CompanyPrice{get;set;}
        public string ProductDescription{get;set;}
        public string CoveragePDFPath{get;set;}
        public string QuantityByLead{get;set;}
        public System.Nullable<bool> IsDefaultPrice{get;set;}
        public int? LinkedReportId{get;set;}
        public string ReportsLinked{get;set;}
        public bool EnableReviewPerCompany{get;set;}
        public string AutoReviewStatusForLocation { get; set; }
        public bool IsEdit { get; set; }
        public int Index { get; set; }
        public byte CategoryId { get; set; }
        public bool IsProductEnabled { get; set; }
        public decimal ? MonthlyVolumeProductWise { get; set; }
        public decimal ? MonthlySaving { get; set; }
        public decimal? MonthlySavinghdn { get; set; }
        public decimal ? CurrentPrice { get; set; }
    }
    public class ScreenModel
    {
        public int fkCompanyId { get; set; }
        public bool Isenable { get; set; }
        public string ProductCode { get; set; }
        public int fkReportCategoryId { get; set; }
        public string accountno { get; set; }
        public string subaccountno { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string pkthirdpartycredentialsId { get; set; }
    }

    public class ScreencatgoriesModel
    {
        public Guid pkthirdpartycredentialsId { get; set; }

        public string ProductCode { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int fkCompanyId { get; set; }
        public int fkReportCategoryId { get; set; }
        public string CategoryName { get; set; }
    }


   





}