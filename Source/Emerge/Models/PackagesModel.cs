﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class PackagesModel
    {
        public string QueryStringPackageId { get; set; }

        public int RowNumber { get; set; }
        public int pkPackageId { get; set; }
        public int OldPackageId { get; set; }
        public int NewPackageId { get; set; }
        public int ParentPackageId { get; set; }
        public bool IsPackageNamedisplay { get; set; }
       

        public int PackageAccessId { get; set; }

        public Guid fkApplicationId { get; set; }
        public int CompanyId { get; set; }
        public int LocationId { get; set; }

        
        public bool IsEnableCompanyPackage { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDelete { get; set; }
        public bool IsPublic { get; set; }
        public bool IsDefault { get; set; }
        public bool is7YearFilterpackage { get; set; }
        public bool is10YearFilterpackage { get; set; }

        [Required(ErrorMessage = "Please enter Package Name")]

        public string PackageName { get; set; }

        [Required(ErrorMessage = "Please enter Package Code")]
        public string PackageCode { get; set; }

        [Required(ErrorMessage = "Please enter Package Display Name")]
        public string PackageDisplayName { get; set; }

       
        public string PackageDescription { get; set; }

        public string CustomMessages { get; set; }

        [Required(ErrorMessage = "Please enter Package Price")]
        [RegularExpression(@"^[0-9]+\.[0-9]{2}$", ErrorMessage = "Enter upto 2 decimal places")]

        public decimal PackagePrice { get; set; }


        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public List<tblProductPackage> lstProductPackages { get; set; }
        public tblProductPackage ObjProductPackages { get; set; }

        public List<Proc_Get_AllEmergeProductsInPackagesResult> lstProductsByPackageId = new List<Proc_Get_AllEmergeProductsInPackagesResult>();


        public bool chkTiered { get; set; }
        public bool IsAutomaticRun { get; set; }
        public string TieredHost { get; set; }
        public string Tiered2 { get; set; }
        public string Tiered3 { get; set; }
        public string TieredOptional { get; set; }
        public int TimeFrame { get; set; }

        [Required(ErrorMessage = "Please Select Tiered Host")]
        public string ddlTieredHost { get; set; }

        [Required(ErrorMessage = "Please Select Tiered2")]
        public string ddlTiered2 { get; set; }

        [Required(ErrorMessage = "Please Select Tiered3")]
        public string ddlTiered3 { get; set; }

        public string ddlTiredOptional { get; set; }

        [Required(ErrorMessage = "Please Select TimeFrame")]
        public string ddlTimeFrame { get; set; }
        public string Tier2Price { get; set; }
        public string Tier3Price { get; set; }

        //[Required(ErrorMessage = "Please enter Host Price")]
        public decimal? HostPrice { get; set; }

        //[Required(ErrorMessage = "Please enter Optional Price")]
        public decimal? OptionalPrice { get; set; }

        public Dictionary<string, string> TieredHostList { get; set; }

        public Dictionary<string, string> Tiered2List { get; set; }

        public Dictionary<string, string> Tiered3List { get; set; }

        public Dictionary<string, string> TieredOptionalList { get; set; }

        public Dictionary<string, string> TimeFrameList { get; set; }
    }
}