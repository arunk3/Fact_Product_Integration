﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using System.ComponentModel.DataAnnotations;
namespace Emerge.Models
{
    public class PenalCodeModel
    {
        public int? pkPenalCodeId { get; set; }
        public string PenalCode { get; set; }
        public string PenalTitle { get; set; }
        public string PenalDescription { get; set; }
        public int? fkStateId { get; set; }
        public string ProductCode { get; set; }
        public string PenalCode_From { get; set; }
        public string PenalCode_To { get; set; }
        public byte? PenalCodeRangeType { get; set; }
        public bool IsNumeric { get; set; }
        public bool IsAlphabet { get; set; }
        public bool IsNoRange { get; set; }
        public List<Filters> FiltersCollection { get; set; }
        [Required(ErrorMessage = "Please enter keywords")]
        public string Keywords { get; set; }

        public int FilterId { get; set; }
        public string StateDropdown { get; set; }
        public string CountyDropdown { get; set; }
        public string DataSourceDropdown { get; set; }
        public string FilterName { get; set; }
        public string ReportType { get; set; }

        public string Key_words { get; set; }
        public string Year { get; set; }
        public string Count { get; set; }
        public string CountMax { get; set; }
        public string CountMin { get; set; }

        public string TimeFrameFrom { get; set; }
        public string TimeFrameIn { get; set; }

        public string UserType { get; set; }
        public bool Packages { get; set; }
        public bool Enabled { get; set; }
        public bool IsEmergeReview { get; set; }
        public bool IsWatchList { get; set; }
        public bool IsRemoveCharges { get; set; }
        public string Filter_Users { get; set; }

    }



    public class Filters
    {
        public string FilterName { get; set; }
        public string ReportType { get; set; }
        public string States { get; set; }
        public string Counties { get; set; }
        public string Coundition1 { get; set; }
        public string Coundition2 { get; set; }
        public string Users { get; set; }
        public bool Packages { get; set; }
        public bool Enabled { get; set; }
        public int fkFilterId { get; set; }
       

    }


    








}