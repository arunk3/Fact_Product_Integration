﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Emerge.Data;

namespace Emerge.Models
{
    public class MarketingToolsModel
    {
        public int FeatureId { get; set; }
        public int ViewCount { get; set; }

        public string Title { get; set; }
        public string UploadedFile { get; set; }
        public string ViewFile { get; set; }
        public string Description { get; set; }
        public bool IsEnabled { get; set; }
        public string CurrentFileName { get; set; }
        public DateTime FeatureCreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
    public class ContentManagementModel
    {
        public int ContentId { get; set; }
        public Guid ApplicationId { get; set; }

        public string ContentTitle { get; set; }
        public string ContentComment { get; set; }
        public bool IsCommentEnabled { get; set; }
        public DateTime ContentCreatedDate { get; set; }
        public DateTime ContentLastModifiedDate { get; set; }
    }
    public class GeneralSettingModel
    {
        public int pkSettingId { get; set; }
        public string CompanyName { get; set; }

        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string SupportEmailId { get; set; }
        public string AdminEmailId { get; set; }
        public string City { get; set; }
        public int State { get; set; }
        public string ZipCode { get; set; }

        public string Fax { get; set; }
        public string WebSiteLogo { get; set; }
        public string PageContentsDefault { get; set; }

        [AllowHtml]       
        public string PageContents { get; set; }
    }
    public class MarketingContentModel
    {
        public MarketingToolsModel MarketingToolsModel { get; set; }
        public ContentManagementModel ContentManagementModel { get; set; }
        public GeneralSettingModel GeneralSettingModel { get; set; }
        public string Message { get; set; }
        public List<tblFeature> ObjFeature { get; set; }

        public List<tblCustomReviewMessage> objtblCustomMsg { get; set; }
        public List<MyFormModelColl> Collection { get; set; }
        public int TotalCounts { get; set; }
        public int RowCounts { get; set; }


        public MarketingContentModel()
        {
            ObjFeature=new List<tblFeature>();
        }
    }

    

}