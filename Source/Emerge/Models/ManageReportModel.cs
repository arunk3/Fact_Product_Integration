﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class ManageReportModel
    {
        public string strMessage { get; set; }


        public int pkReportCategoryId
        {
            get;
            set;

        }
        public string ProductCode
        {
            get;
            set;
        }
        public string CategoryName
        {
            get;
            set;
        }
        public string ProductDisplayName
        {
            get;
            set;
        }

        public string ProductDescription
        {
            get;
            set;
        }
        public string CoveragePDFPath
        {
            get;
            set;
        }



        public bool IsEnabled
        {
            get;
            set;
        }

        public bool IsDefault
        {
            get;
            set;
        }

        public bool IsGlobal
        {
            get;
            set;
        }

        public bool DoNotShow
        {
            get;
            set;
        }
        public string ProductName
        {
            get;
            set;
        }

        public string VendorName
        {
            get;
            set;
        }

        public decimal ProductPerApplicationPrice
        {
            get;
            set;
        }

        public bool IsEnableReview
        {
            get;
            set;
        }
        public int pkProductId
        {
            get;
            set;
        }

        public int pkid
        {
            get;
            set;
        }

        public int fkVendorId
        {
            get;
            set;
        }
        public string Code
        {
            get;
            set;
        }
        public bool IsConsent
        {
            get;
            set;
        }

        public int pkStateId
        {
            get;
            set;
        }


        public string StateName
        {
            get;
            set;
        }

        public decimal AdditionalFee
        {
            get;
            set;
        }

        public bool LiveRunnerFees
        {
            get;
            set;
        }
        public bool IsLiveRunner
        {
            get;
            set;
        }
        public int pkProductApplicationId
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Please select file")]
        public HttpPostedFileBase CoveragePDFFile { get; set; }


        List<Proc_Get_EmergeReportsForAdminResult> GetEmergeReportForAdmin
        {
            get;
            set;

        }


        public List<Proc_GetProductDescrptionResult> GetProductDescrption1
        {
            get;
            set;
        }

        public List<Proc_Get_EmergeReportsForAdminResult> GettblProductReport
        {
            get;
            set;
        }


    }


    public class ReportCommentModel
    {
        public int ReportCommentsId { get; set; }
        public string ButtonTitle { get; set; }
        public string Comments { get; set; }
        public string Message { get; set; }
        public int LineNo { get; set; }
        public List<tblLinesForReportComment> LineNumberDataSource { get; set; }
        public List<Proc_GetLineReportCommentsResult> Rows { get; set; }

    }
}