﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace VendorServices
{
    public class RapidCourtService
    {
        DirectconnectServiceProxy ObjDirectconnectServiceProxy;
        public Dictionary<string, string> RunReport(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            ObjDirectconnectServiceProxy = new DirectconnectServiceProxy();

            string backgroundReportXML = "";
            try
            {
                string XMLString = ObjGeneralService.Request;

                string username = ObjGeneralService.ServiceUserId.ToString();
                string password = ObjGeneralService.ServicePassword.ToString();

                XmlDocument ObjXmlDocument = new XmlDocument();

                string _X = ObjGeneralService.AdditionalParams;
                ObjXmlDocument.LoadXml(_X);
                XmlNode ObjXmlNode = ObjXmlDocument.DocumentElement;
                string accountid = ObjXmlNode.SelectSingleNode("accountId").ChildNodes[0].Value;
                if (ObjGeneralService.ProductCode.ToLower() == "rcx")
                {
                    //As per Greg, he needs to stop count of Liverunner products to calling the vendor service 3 times.
                    //Code Blocked for INT-183 LiveRunner Report gets Incomplete.
                    int counter = 0;
                    //for testing
                    if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                    {
                        string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "RapidCourt.txt";

                        if (System.IO.File.Exists(file_path))
                        {
                            backgroundReportXML = File.ReadAllText(file_path);
                        }
                    }
                    else
                    {
                        backgroundReportXML = ObjDirectconnectServiceProxy.runSearch(username, password, accountid, XMLString).ToString();
                    }
                    GeneralService.WriteLog(Environment.NewLine + "RunReport_" + counter.ToString() + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XMLString, "RapidCourt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                    //For testing environment.
                    if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                    {
                        string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "RapidCourt.txt";
                        if (System.IO.File.Exists(file_path))
                        {
                            backgroundReportXML = File.ReadAllText(file_path);
                        }
                    }
                    else
                    {//For Live environment.
                        backgroundReportXML = ObjDirectconnectServiceProxy.runSearch(username, password, accountid, XMLString).ToString();
                    }
                    GeneralService.WriteLog(Environment.NewLine + "RunReport_" + counter.ToString() + "Response: (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + backgroundReportXML, "RapidCourt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                }
                else//Except Live Runner products. 
                {
                    //GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XMLString, "RapidCourt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                    //For testing enviroment.
                    if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                    {
                        string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "RapidCourt.txt";
                        if (System.IO.File.Exists(file_path))
                        {
                            backgroundReportXML = File.ReadAllText(file_path);
                        }
                    }
                    else//For Live environment.
                    {
                        backgroundReportXML = ObjDirectconnectServiceProxy.runSearch(username, password, accountid, XMLString).ToString();
                    }
                    //GeneralService.WriteLog(Environment.NewLine + "Response (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + backgroundReportXML, "RapidCourt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                }
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "RapidCourt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = backgroundReportXML;
            return Response_Dic;
        }

        /// <summary>
        /// Function to calling vendor service after second time.
        /// </summary>       
        public Dictionary<string, string> RunReport_ForPendingReports(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            ObjDirectconnectServiceProxy = new DirectconnectServiceProxy();
            string backgroundReportXML = "";
            try
            {
                string XMLString = ObjGeneralService.Request;
                string username = ObjGeneralService.ServiceUserId.ToString();
                string password = ObjGeneralService.ServicePassword.ToString();
                XmlDocument ObjXmlDocument = new XmlDocument();
                string _X = ObjGeneralService.AdditionalParams;
                ObjXmlDocument.LoadXml(_X);
                XmlNode ObjXmlNode = ObjXmlDocument.DocumentElement;
                string accountid = ObjXmlNode.SelectSingleNode("accountId").ChildNodes[0].Value;
                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + XMLString, "RapidCourt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                backgroundReportXML = ObjDirectconnectServiceProxy.runSearch(username, password, accountid, XMLString).ToString();
                GeneralService.WriteLog(Environment.NewLine + "Response: (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + backgroundReportXML, "RapidCourt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_ForPendingReports_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "RapidCourt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {
                Response_Dic["response"] = backgroundReportXML;
            }
            return Response_Dic;
        }
    }
}
