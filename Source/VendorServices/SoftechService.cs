﻿using System;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using System.Collections.Generic;
using System.IO;
namespace VendorServices
{
    public class SoftechService
    {

        #region /// Code To run the Actual Report

        /// <summary>
        /// Function To Run the Softech Report
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            Response_Dic["ispending"] = "";
            string XmlResponse = "";
            try
            {
                string Request = ObjGeneralService.Request;

                string[] Request_Part = Request.Split('?');

                while (XmlResponse == "")
                {
                    GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + Request, "Softech/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);


                    //for testing
                    if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                    {
                        string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "Softtech.txt";
                        if (System.IO.File.Exists(file_path))
                        {
                            XmlResponse = File.ReadAllText(file_path);
                        }
                    }
                    else
                    {
                        XmlResponse = ObjGeneralService.HttpRequest("POST", Request_Part[0].ToString(), Request_Part[1].ToString(), 120000);
                    }

                    GeneralService.WriteLog(Environment.NewLine + "Response (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XmlResponse, "Softech/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                }

                if (ObjGeneralService.IsValidXml(XmlResponse) == true)
                {
                    XDocument ObjXDocument = XDocument.Parse(XmlResponse);
                    var Nodes_request_status = ObjXDocument.Descendants("MvrStatus").ToList();
                    var Nodes_key = ObjXDocument.Descendants("Key").ToList();
                    var Nodes_quoteback = ObjXDocument.Descendants("QuoteBack").ToList();
                    if (Nodes_request_status.Count != 0)
                    {
                        foreach (XElement ObjXElement in Nodes_request_status)
                        {
                            if (ObjXElement.Value == "3" || ObjXElement.Value == "4" || ObjXElement.Value == "5" || ObjXElement.Value == "6")
                            {
                                if (Nodes_key.Count != 0)
                                {
                                    string key = "";
                                    key = Nodes_key[0].Value;
                                    XmlResponse += "^" + key;
                                }
                                break;
                            }
                            else
                            {
                                if (Nodes_key.Count != 0)
                                {
                                    string key = "";
                                    key = Nodes_key[0].Value;
                                    XmlResponse += "^" + key;
                                }
                                Response_Dic["ispending"] = "true";
                                break;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "Softech/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = XmlResponse;
            return Response_Dic;
        }

        #endregion

        /// <summary>
        /// Function to Run the Pending Report
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport_ForPendingReports(GeneralService ObjGeneralService)
        {
            //MVR_Non_Instant
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            Response_Dic["ispending"] = "";
            string XmlResponse = "";
            try
            {
                string[] Request = ObjGeneralService.SendRequestForNonInstantMVR();
                XmlResponse = ObjGeneralService.HttpRequest("POST", Request[0], Request[1], 120000);

                if (ObjGeneralService.IsValidXml(XmlResponse) == true)
                {

                    XDocument ObjXDocument = XDocument.Parse(XmlResponse);
                    var filename = ObjXDocument.Descendants("QuoteBack").ToList();
                    if (filename.Count != 0)
                    {
                        foreach (XElement ObjXElementfile in filename)
                        {
                            GeneralService.WriteLog(Environment.NewLine + "Response (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + XmlResponse, "Softech/" + Convert.ToString(ObjXElementfile.Value));
                            var Nodes_request_status = ObjXDocument.Descendants("MvrStatus").ToList();
                            var Nodes_key = ObjXDocument.Descendants("Key").ToList();
                            var Nodes_quoteback = ObjXDocument.Descendants("QuoteBack").ToList();

                            if (Nodes_request_status.Count != 0)
                            {
                                foreach (XElement ObjXElement in Nodes_request_status)
                                {
                                    if (ObjXElement.Value == "3" || ObjXElement.Value == "4" || ObjXElement.Value == "5" || ObjXElement.Value == "6")
                                    {
                                        if (Nodes_key.Count != 0)
                                        {
                                            string key = "";
                                            key = Nodes_key[0].Value;
                                            XmlResponse += "^" + key;
                                        }
                                        break;
                                    }
                                    else
                                    {
                                        if (Nodes_key.Count != 0)
                                        {
                                            string key = "";
                                            key = Nodes_key[0].Value;
                                            XmlResponse += "^" + key;
                                        }
                                        Response_Dic["ispending"] = "true";
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_ForPendingReports_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "Softech/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = XmlResponse;
            return Response_Dic;
        }
    }
}
