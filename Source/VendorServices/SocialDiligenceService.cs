﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace VendorServices
{
    public class SocialDiligenceService
    {
        /// <summary>
        /// Function to Run the Social Diligence Report .
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            string XmlResponse = string.Empty;
            try
            {
                string XMLString = ObjGeneralService.Request;
                string[] XMLString_Part = XMLString.Split('?');

                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XMLString, "SocialDiligence/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);

                //for testing
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                {
                    string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "SocialDiligence.txt";                    
                    if (System.IO.File.Exists(file_path))
                    {
                        XmlResponse = File.ReadAllText(file_path);
                    }
                }
                else
                {
                    XmlResponse = ObjGeneralService.SendRequest(XMLString_Part[0].ToString(), XMLString_Part[1].ToString());
                }

                GeneralService.WriteLog(Environment.NewLine + "Response (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XmlResponse, "SocialDiligence/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "SocialDiligence/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = XmlResponse;
            return Response_Dic;
        }

        /// <summary>
        /// Function to Run the Pending Report
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport_ForPendingReports(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            Response_Dic["response"] = ObjGeneralService.Response;
            return Response_Dic;
        }
    }
}
