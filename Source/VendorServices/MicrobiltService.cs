﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace VendorServices
{
    public class MicrobiltService
    {
        /// <summary>
        /// Function To Run Microbilt Report .
        /// </summary>
        /// <param name="ObjGetOrderSearchedDataResult"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            string Request = "";
            string Response = "";
            string OriginalResponse = "";
            string OriginalRequestContent = "";
            string SecondRequest = "";
            string Microbilt_Response = "";
            try
            {
                Request = ObjGeneralService.Request;
                string[] Request_Part = Request.Split('?');

                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGetOrderSearchedDataResult)\n" + Environment.NewLine + Request, "Microbilt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);


                //for testing
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                {
                    string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "MicrobiltService.txt";                    
                    if (System.IO.File.Exists(file_path))
                    {
                        Response = File.ReadAllText(file_path);
                        OriginalResponse = Response;
                    }
                }
                else
                {
                    Response = ObjGeneralService.SendRequest(Request_Part[0].ToString() + "?", Request_Part[1].ToString());
                    GeneralService.WriteLog(Environment.NewLine + "Response: (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGetOrderSearchedDataResult)\n" + Environment.NewLine + Response, "Microbilt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);

                    OriginalRequestContent = GetOriginalResponse(Response, ObjGeneralService);

                    SecondRequest = "https://creditserver.microbilt.com/SDK/XMLInterface2.asp?" + OriginalRequestContent;

                    int counter = 0;

                    OriginalResponse = "<XML_INTERFACE>ZZZ</XML_INTERFACE>";
                    /*counter <= 6*/
                    while (counter <= 3 && OriginalResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
                    {
                        GeneralService.WriteLog(Environment.NewLine + "RunReport_" + counter.ToString() + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGetOrderSearchedDataResult)\n" + Environment.NewLine + SecondRequest, "Microbilt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);

                        OriginalResponse = ObjGeneralService.SendRequest("https://creditserver.microbilt.com/SDK/XMLInterface2.asp", OriginalRequestContent);

                        GeneralService.WriteLog(Environment.NewLine + "RunReport_" + counter.ToString() + "Response: (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGetOrderSearchedDataResult)\n" + Environment.NewLine + OriginalResponse, "Microbilt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                        if (OriginalResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
                        {
                            System.Threading.Thread.Sleep(20000);/*4000  Changed Date 12Sept2011, by Ref Ticket 622*/
                            counter++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

               
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "Microbilt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {
                if (OriginalResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
                {
                    Microbilt_Response = SecondRequest + "#" + OriginalResponse;
                }
                else
                {
                    Microbilt_Response = OriginalResponse;
                }
            }
            Response_Dic["response"] = Microbilt_Response;
            return Response_Dic;
        }

        /// <summary>
        /// Function to Get Actual Response 
        /// </summary>
        /// <param name="Response"></param>
        /// <param name="ObjGetOrderSearchedDataResult"></param>
        /// <returns></returns>
        private string GetOriginalResponse(string Response, GeneralService ObjGeneralService)
        {
            StringBuilder ObjXML = new StringBuilder();

            ObjXML.Append("MemberID=" + ObjGeneralService.ServiceUserId + "&Password=" + ObjGeneralService.ServicePassword);
            ObjXML.Append("&GUID=" + Response);
            ObjXML.Append("&DecFile=&OutputFormat=3&ckDecisionTable=0&ckRAWData=1&ckCommonFormat=1&ckXML=1&submit1=Submit");

            return ObjXML.ToString();
        }

        /// <summary>
        /// Function to Run the Pending Report
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport_ForPendingReports(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            int counter = 0;
            string Request = "";
            string OriginalResponse = "";
            try
            {
                Request = ObjGeneralService.Request;
                OriginalResponse = ObjGeneralService.Response;
                string[] Request_Part = Request.Split('?');

                while (counter <= 6 && OriginalResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
                {
                    GeneralService.WriteLog(Environment.NewLine + "RunReport_ForPendingReports_" + counter.ToString() + "Request (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + Request, "Microbilt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);

                    OriginalResponse = ObjGeneralService.SendRequest(Request_Part[0].ToString() + "?", Request_Part[1].ToString());

                    GeneralService.WriteLog(Environment.NewLine + "RunReport_ForPendingReports_" + counter.ToString() + "Response (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + OriginalResponse, "Microbilt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);

                    System.Threading.Thread.Sleep(4000);
                    counter++;
                }
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = "<XML_INTERFACE>ZZZ</XML_INTERFACE>";
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_ForPendingReports_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "Microbilt/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }

            Response_Dic["response"] = OriginalResponse;
            return Response_Dic;
        }
    }
}
