﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using System.Data.Linq;
using System.Xml.Linq;
using VendorServices;
using System.Xml;
using System.Net.Mail;
using System.IO;

namespace VendorServices
{
    public class BackgroundChecksService
    {
        string XmlResponse = "";

        #region /// Code To run the Actual Report

        /// <summary>
        /// Function to Run the Background Report .
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            string SecureKey = "";
            try
            {
                string XMLString = ObjGeneralService.Request;
                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XMLString, "BackgroundChecks/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);

                //for testing
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                {
                    string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "BackgroundCheck.txt";
                    
                    if (System.IO.File.Exists(file_path))
                    {
                        XmlResponse = File.ReadAllText(file_path);
                    }
                }
                else
                {
                    XmlResponse = ObjGeneralService.SendRequest(ObjGeneralService.ServiceUrl + "?", XMLString);
                }

                GeneralService.WriteLog(Environment.NewLine + "Response (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XmlResponse, "BackgroundChecks/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                XDocument ObjXDocument = XDocument.Parse(XmlResponse);
                var List_OrderStatus = ObjXDocument.Descendants("secureKey").ToList();
                foreach (XElement ObjXElement in List_OrderStatus)
                {
                    if (ObjXElement != null)
                    {
                        SecureKey = ObjXElement.Value.Trim();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "BackgroundChecks/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = XmlResponse + "^" + SecureKey;
            return Response_Dic;
        }
        #endregion


        /// <summary>
        /// Function to Run the Pending Report
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport_ForPendingReports(GeneralService ObjGeneralService)
        {

            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            string XmlResponse_Pending = ObjGeneralService.Response;
            try
            {
                if (ObjGeneralService.RefSecureKey != "")
                {
                    //GeneralService.SendMail("Entered To RunReport_ForPendingReports Function");

                    XmlDocument ObjXmlDocument_AdditionalParams = new XmlDocument();

                    ObjXmlDocument_AdditionalParams.LoadXml(ObjGeneralService.AdditionalParams);

                    XmlNode ObjXmlNode_AdditionalParam = ObjXmlDocument_AdditionalParams.SelectSingleNode("//secondRequestUrl");

                    string secondRequestUrl = "";

                    if (ObjXmlNode_AdditionalParam != null)
                    {
                        secondRequestUrl = ObjXmlNode_AdditionalParam.InnerText;
                    }

                    XmlNode ObjXmlNode_Account = ObjXmlDocument_AdditionalParams.SelectSingleNode("//accountId");

                    string AccountID = "";

                    if (ObjXmlNode_Account != null)
                    {
                        AccountID = ObjXmlNode_Account.InnerText;
                    }

                    StringBuilder ObjStringBuilder = new StringBuilder();

                    ObjStringBuilder.Append("<?xml version='1.0' encoding='us-ascii' standalone='yes'?>");
                    ObjStringBuilder.Append("<BGC version='1.4'>");
                    ObjStringBuilder.Append("<login>");
                    ObjStringBuilder.Append("<user>" + ObjGeneralService.ServiceUserId + "</user>");
                    ObjStringBuilder.Append("<password>" + ObjGeneralService.ServicePassword + "</password>");
                    ObjStringBuilder.Append("<account>" + AccountID + "</account>");
                    ObjStringBuilder.Append("</login>");
                    ObjStringBuilder.Append("<product>");

                    string Parameter = "";

                    switch (ObjGeneralService.ProductCode.Trim())
                    {
                        case "SCR":
                            Parameter = "StatewideCriminalSearch";
                            break;
                        case "CCR2":
                            Parameter = "CountyCriminalSearch";
                            break;
                    }

                    ObjStringBuilder.Append("<" + Parameter + " " + (Parameter == "StatewideCriminalSearch" ? "version='1'" : "") + ">");
                    ObjStringBuilder.Append("<fulfill>");
                    ObjStringBuilder.Append("<secureKey>" + ObjGeneralService.RefSecureKey + "</secureKey>");
                    ObjStringBuilder.Append("</fulfill>");
                    ObjStringBuilder.Append("</" + Parameter + ">");
                    ObjStringBuilder.Append("</product>");
                    ObjStringBuilder.Append("</BGC>");

                    string Request_Second = ObjStringBuilder.ToString();
                    GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + Request_Second, "BackgroundChecks/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                    XmlResponse_Pending = ObjGeneralService.SendRequest(secondRequestUrl, Request_Second);
                    GeneralService.WriteLog(Environment.NewLine + "Response: (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + XmlResponse_Pending, "BackgroundChecks/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                    //GeneralService.SendMail("Request is : \t :" + Request_Second + "\n Response id : \t :" + XmlResponse);
                }
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_ForPendingReports_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "BackgroundChecks/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = XmlResponse_Pending;
            return Response_Dic;
        }

    }
}
