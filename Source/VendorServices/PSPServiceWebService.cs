﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dot.Psp.Common;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Web;

namespace VendorServices
{
    public class PSPServiceWebService
    {
        public Dictionary<string, string> RunReport(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            Response_Dic["ispending"] = "";
            string XmlResponse = "";
            try
            {
                string Request = ObjGeneralService.Request;
                string[] Request_Part = Request.Split('?');
                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + Request, "PSPVendorCall/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                //for testing
                System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"] = "false";
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                {
                    string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "PSPService.txt";
                    if (System.IO.File.Exists(file_path))
                    {
                        string tempXmlResponse = File.ReadAllText(file_path);
                        int errorindex1 = tempXmlResponse.IndexOf("<success>");
                        int errorindex2 = tempXmlResponse.IndexOf("</success>");
                        string errorXML1 = tempXmlResponse.Substring(errorindex1 + 9, errorindex2 - errorindex1 - 9);
                        if (errorXML1.ToLower() != "no")
                        {
                            int firstIndex = tempXmlResponse.IndexOf("<ProductOrderresponseString>");
                            int lastIndex = tempXmlResponse.IndexOf("</ProductOrderresponseString>");
                            string FinalXML1 = tempXmlResponse.Substring(firstIndex, lastIndex - firstIndex);
                            int Qindex = FinalXML1.IndexOf('?');
                            string FinalXML2 = FinalXML1.Substring(Qindex + 1, FinalXML1.Length - 29);
                            string str = HttpUtility.HtmlDecode(FinalXML2);
                            XmlResponse = str;
                        }
                        else
                        {
                            XmlResponse = HttpUtility.HtmlDecode(tempXmlResponse);
                        }

                    }
                }
                else
                {
                    string tempXmlResponse = ObjGeneralService.SendRequestforPSP(ObjGeneralService.Request);

                    int errorindex1 = tempXmlResponse.IndexOf("<success>");
                    int errorindex2 = tempXmlResponse.IndexOf("</success>");
                    string errorXML1 = tempXmlResponse.Substring(errorindex1 + 9, errorindex2 - errorindex1 - 9);
                    if (errorXML1.ToLower() != "no")
                    {
                        int firstIndex = tempXmlResponse.IndexOf("<ProductOrderresponseString>");
                        int lastIndex = tempXmlResponse.IndexOf("</ProductOrderresponseString>");
                        string FinalXML1 = tempXmlResponse.Substring(firstIndex, lastIndex - firstIndex);
                        //int Qindex = FinalXML1.IndexOf('?');
                        int Qindex = FinalXML1.IndexOf('>');
                        string FinalXML2 = FinalXML1.Substring(Qindex + 2, FinalXML1.Length - (Qindex + 2));
                        string str = HttpUtility.HtmlDecode(FinalXML2);
                        XmlResponse = str;
                    }
                    else
                    {
                        XmlResponse = HttpUtility.HtmlDecode(tempXmlResponse);
                    }

                }

                GeneralService.WriteLog(Environment.NewLine + "Response (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XmlResponse, "PSPVendorCall/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "PSPVendorCall/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = XmlResponse;
            return Response_Dic;
        }
    }
}
