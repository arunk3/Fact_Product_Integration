﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Net.Mail;
using System.IO;




namespace VendorServices
{
    public class XpediteService
    {
        FastraxNetworkProxy ObjFastraxNetwork;

        #region /// Code To run the Actual Report

        /// <summary>
        /// Function To Run the Xpedite Report
        /// </summary>
        /// <param name="ObjGetOrderSearchedDataResult"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            string resultsXml = "";
            ObjFastraxNetwork = new FastraxNetworkProxy();
            List<string> ObjList = new List<string>();
            string requestId = "";
            string responseXml = string.Empty;
            try
            {
                CheckLoginCred(ObjGeneralService);

                string XMLString = ObjGeneralService.Request;
                //for testing
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                {
                    string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "Xpedite.txt";                    
                    if (System.IO.File.Exists(file_path))
                    {
                        responseXml = File.ReadAllText(file_path);
                    }
                }
                else
                {
                    responseXml = ObjFastraxNetwork.InsertRequests(XMLString);
                }

                XmlDocument xmlResponse = new XmlDocument();

                xmlResponse.LoadXml(responseXml);
                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XMLString, "Xpedite/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                GeneralService.WriteLog(Environment.NewLine + "Response: (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + responseXml, "Xpedite/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                requestId = xmlResponse.DocumentElement.SelectSingleNode(@"/FastraxNetwork/request/@request-id").Value;
                string xmlString_Another = @"<?xml version=""1.0"" encoding=""utf-8"" ?><FastraxNetwork><search-criteria><request-id>" + requestId + "</request-id></search-criteria></FastraxNetwork>";
                if (ObjGeneralService.ProductCode == "NCR2")
                {
                    string responseXml_ncr = "<FastraxNetwork><report-requested><report><status>R</status></report></report-requested><client-fields></client-fields></FastraxNetwork>";
                    XmlDocument xmlResponse_ncr = new XmlDocument();
                    xmlResponse_ncr.LoadXml(responseXml_ncr);
                    string ncr1_status = xmlResponse_ncr.DocumentElement.SelectSingleNode("/FastraxNetwork/report-requested/report/status").InnerText;
                    int Counter_Ncr = 0;
                    while (ncr1_status == "R" && Counter_Ncr < 12)
                    {
                        resultsXml = ObjFastraxNetwork.RetrieveResults(xmlString_Another);
                        xmlResponse_ncr.LoadXml(resultsXml);
                        ncr1_status = xmlResponse_ncr.DocumentElement.SelectSingleNode("/FastraxNetwork/report-requested/report/status").InnerText;                    
                        System.Threading.Thread.Sleep(5000);
                        Counter_Ncr++;
                    }
                }
                else
                {
                    resultsXml = responseXml;  //For CCR1 Report 
                }
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "Xpedite/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = resultsXml + "^" + requestId;
            return Response_Dic;
        }
        #endregion

        #region /// Function To check Login Credentials

        /// <summary>
        /// Function To Login 
        /// </summary>
        public void CheckLoginCred(GeneralService ObjGetOrderSearchedDataResult)
        {
            ObjFastraxNetwork = new FastraxNetworkProxy();
            try
            {
                if (ObjGetOrderSearchedDataResult.ServiceUrl.ToString() != "")
                {
                    ObjFastraxNetwork.Url = ObjGetOrderSearchedDataResult.ServiceUrl.ToString();
                }
                string CustomerLocation = ObjGetOrderSearchedDataResult.ServiceUserId.ToString();
                string password = ObjGetOrderSearchedDataResult.ServicePassword.ToString();
                ObjFastraxNetwork.FastraxLoginValue = ObjFastraxNetwork.Login(CustomerLocation, password);
                string CheckLogin = Convert.ToString(ObjFastraxNetwork.FastraxLoginValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //ObjFastraxNetwork = null;                
            }
        }
        #endregion

        /// <summary>
        /// Function to Run the Pending Report
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        ///  RunReport(GeneralService ObjGeneralService)


        public Dictionary<string, string> RunReport_ForPendingReports(GeneralService ObjGeneralService)
        {

            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";

            string resultsXml = "";
            try
            {
                CheckLoginCred(ObjGeneralService);

                string XMLString = ObjGeneralService.Request;

                string requestId = ObjGeneralService.RefSecureKey;

                //Format an Xml document, use a Request ID to search and retrieve results of the Request
                string xmlString_Another = @"<?xml version=""1.0"" encoding=""utf-8"" ?><FastraxNetwork><search-criteria><request-id>" + requestId + "</request-id></search-criteria></FastraxNetwork>";

                resultsXml = ObjFastraxNetwork.RetrieveResults(xmlString_Another);

                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + xmlString_Another, "Xpedite/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                GeneralService.WriteLog(Environment.NewLine + "Response: (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + resultsXml, "Xpedite/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);

            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_ForPendingReports_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "Xpedite/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {

            }
            Response_Dic["response"] = resultsXml;
            return Response_Dic;
        }
    }
}
