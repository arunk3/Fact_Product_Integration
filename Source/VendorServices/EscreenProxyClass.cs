﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("WebMatrix", "0.6.0.0")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Web.Services.WebServiceBindingAttribute(Name = "SingleSignOnSoap", Namespace = "http://services.escreen.com/")]
public partial class SingleSignOn : System.Web.Services.Protocols.SoapHttpClientProtocol
{

    /// <remarks/>
    public SingleSignOn()
    {
        this.Url = "https://services.escreen.com/singlesignon/singlesignon.asmx";
    }

    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://services.escreen.com/RequestTicket", RequestNamespace = "http://services.escreen.com/", ResponseNamespace = "http://services.escreen.com/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public string RequestTicket(string sRequestXML)
    {
        object[] results = this.Invoke("RequestTicket", new object[] {
                    sRequestXML});
        return ((string)(results[0]));
    }

    /// <remarks/>
    public System.IAsyncResult BeginRequestTicket(string sRequestXML, System.AsyncCallback callback, object asyncState)
    {
        return this.BeginInvoke("RequestTicket", new object[] {
                    sRequestXML}, callback, asyncState);
    }

    /// <remarks/>
    public string EndRequestTicket(System.IAsyncResult asyncResult)
    {
        object[] results = this.EndInvoke(asyncResult);
        return ((string)(results[0]));
    }
}
