﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System.Net.Mail;
using System.IO;
using System.Text;

namespace VendorServices
{
    public class FRSService
    {
        /// <summary>
        /// To create the final request format for vendor service.//INT336
        /// </summary>        
        public string getFinalRequest(GeneralService ObjGeneralService)
        {
            string requestdata = string.Empty;
            try
            {
                requestdata = new GeneralService().CreateRequestForVerification(ObjGeneralService);
            }
            catch (Exception)
            {

                throw;
            }
            return requestdata;
        }
        /// <summary>
        /// Function to call the vendor serive at first final request.//INT336
        /// </summary>        
        public Dictionary<string, string> RunReport(GeneralService ObjGeneralService)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            string XmlResponse = "";
            try
            {
                string XMLString = ObjGeneralService.Request;

                //For testing environment.
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]))
                {
                    string file_path = System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode-filePath"] + "FRSService.txt";
                    if (System.IO.File.Exists(file_path))
                    {
                        XmlResponse = File.ReadAllText(file_path);
                    }
                }
                else//For Live enviroment.
                {
                    XmlResponse = ObjGeneralService.SendRequestForVerification(ObjGeneralService);
                }
                GeneralService.WriteLog(Environment.NewLine + "Response: (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + XmlResponse, "FRS/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "FRS/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            finally
            {
                Response_Dic["response"] = XmlResponse;
            }
            return Response_Dic;
        }

        /// <summary>
        /// Function to call vendor serice again for pending orders.//INT336
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public List<FRSAttributes> RunReport_ForPendingReports(GeneralService ObjGeneralService, string ids)
        {
            List<FRSAttributes> ObjFRSAttributes = new List<FRSAttributes>();

            string XmlResponse = string.Empty;
            try
            {
                string Request = ObjGeneralService.Request;
                XmlResponse = string.Empty;
                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + Request, "FRS/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                System.Threading.Thread.Sleep(2000);
                XmlResponse = ObjGeneralService.SendRequestForVerification(ObjGeneralService);

                ObjFRSAttributes.Add(GetSingleFrsObject(XmlResponse, ObjGeneralService.ProductCode, ObjGeneralService.OrderId));
                GeneralService.WriteLog(Environment.NewLine + "Response: (" + DateTime.Now.ToString() + "):RunReport_ForPendingReports(GeneralService ObjGeneralService)\n" + Environment.NewLine + XmlResponse, "FRS/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
            }
            catch (Exception ex)
            {
                GeneralService.WriteLog(Environment.NewLine + "Error RunReport_ForPendingReports_" + ex.Message.ToString() + "Response: (" + DateTime.Now.ToString() + "):\n", "FRS/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                WCVLog(DateTime.Now, "3. RunReport_ForPendingReports", ex.Message.ToString());
            }
            finally
            {

            }
            return ObjFRSAttributes;
        }
        public void SendErrorMail(string errormsg, string subject)
        {
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                List<string> BCC_List = new List<string>();                
                ObjMailProvider.Message = errormsg;
                ObjMailProvider.MailFrom = "emerge@intelifi.com";
                ObjMailProvider.MailBCC = BCC_List;
                ObjMailProvider.Subject = subject;
                ObjMailProvider.IsBodyHtml = true;
                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch (Exception ex2)
            {

            }
        }

        public void WCVLog(DateTime CreatedDate, string name, string value)
        {
            try
            {
                //Do nothing.
            }
            catch
            { }

        }

        public FRSAttributes GetSingleFrsObject(string Response, string productCode, string OrderId)
        {

            FRSAttributes ObjFRSAttributes_Single = new FRSAttributes();
            ObjFRSAttributes_Single.FRS_Order_Id = OrderId;
            ObjFRSAttributes_Single.FRS_Product_Code = productCode;
            ObjFRSAttributes_Single.FRS_Response = Response;

            WCVLog(DateTime.Now, "2.1 GetSingleFrsObject", ObjFRSAttributes_Single.FRS_Response.ToString());
            return ObjFRSAttributes_Single;
        }


    }

    public class FRSAttributes
    {
        public string FRS_Response { get; set; }
        public string FRS_Product_Code { get; set; }
        public string FRS_Order_Id { get; set; }
        public string FRS_Error { get; set; }
    }
}
