﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;

namespace VendorServices
{
    // int 176 escreen 
    public class EscreenService
    {

        SingleSignOn ObjSingleSignOn;

        #region /// Code To run the Actual Report

        /// <summary>
        /// Function To run Rapid court Reports 
        /// </summary>
        /// <param name="ObjGeneralService"></param>
        /// <returns></returns>
        public Dictionary<string, string> RunReport(string Request)
        {
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            Response_Dic["response"] = "";
            Response_Dic["error"] = "";
            ObjSingleSignOn = new SingleSignOn();
            string EscreenReportXML = "";
            string logfilename = "Escreen/" + DateTime.Now.Ticks;
            try
            {

                string certPath = ConfigurationManager.AppSettings["eScreenCertificatePath"].ToString();
                GeneralService.WriteLog("\nCertificate Path: " + certPath, logfilename);
                if (certPath != "")
                {
                    X509Certificate2 cert = new X509Certificate2(certPath, "UvpE@1Va3m", X509KeyStorageFlags.MachineKeySet);
                    ObjSingleSignOn.ClientCertificates.Add(cert);
                    GeneralService.WriteLog("\nCertificate Name: " + cert.FriendlyName, logfilename);
                }
                EscreenReportXML = ObjSingleSignOn.RequestTicket(Request.ToString());
                GeneralService.WriteLog("\nRequest XML Details: \n" + Request, logfilename);
                GeneralService.WriteLog("\n Response XML Details: \n" + EscreenReportXML, logfilename);

            }
            catch (Exception ex)
            {
                Response_Dic["error"] = ex.Message.ToString();
                GeneralService.WriteLog("\nException Details: \n" + ex.Message.ToString(), logfilename);
            }
            finally
            {

            }
            Response_Dic["response"] = EscreenReportXML;
            return Response_Dic;

        }

        #endregion
    }
}
