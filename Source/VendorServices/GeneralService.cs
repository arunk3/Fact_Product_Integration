﻿using System.Net;
using System.IO;
using System.Xml;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System;
using System.Data.Linq;
using System.Xml.Linq;
namespace VendorServices
{
    public class GeneralService
    {
        #region //List of Parameters

        //-----------   Search Parameters-----------------//


        public string Request { get; set; }
        public string Response { get; set; }
        public string AdditionalParams { get; set; }
        public string ServicePassword { get; set; }
        public string ServiceUrl { get; set; }
        public string ServiceUserId { get; set; }
        public string ProductCode { get; set; }
        public string RefSecureKey { get; set; }
        public string IsMissingField { get; set; }
        public string OrderId { get; set; }
        public string OrderDetailId { get; set; }


        //-------   Search Parameters-------------END----//

        #endregion

        #region /// Code To Send HttpRequest

        /// <summary>
        /// Function to Send Request To the Online Web Services And Fetch Data From Them
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public string SendRequest(string uri, string content)
        {
            if (uri == "")
            {
                return "";
            }


            string Response = "";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Timeout = 65000;
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = WebRequestMethods.Http.Post;
            request.ContentLength = content.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(content);
            writer.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            Response = reader.ReadToEnd();
            response.Close();

            return Response;
        }
        #endregion

        #region //Function To Check Valid XML

        public bool IsValidXml(string XMLString)
        {
            try
            {
                XmlDocument ObjXmlDocument = new XmlDocument();
                ObjXmlDocument.LoadXml(XMLString);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        #endregion

        /// <summary>
        /// Function to Send an email
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <returns></returns>
        public static bool SendMail(string MESSAGE)
        {
            int SendingStatus = 0;

            MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

            List<string> BCC_List = new List<string>();
            BCC_List.Add(ConfigurationManager.AppSettings["test_mail_bcc"].ToString());

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(ConfigurationManager.AppSettings["test_mail_to"].ToString());
            ObjMailProvider.MailFrom = ConfigurationManager.AppSettings["test_mail_from"].ToString();
            ObjMailProvider.MailBCC = BCC_List;
            ObjMailProvider.Subject = " Check Report Response From Emerge ";
            ObjMailProvider.IsBodyHtml = false;
            ObjMailProvider.Priority = MailPriority.High;

            if (ConfigurationManager.AppSettings["is_send_test_mail"].ToString() == "true")
            {
                ObjMailProvider.SendMail(out SendingStatus);
            }

            return (SendingStatus == 1 ? true : false);
        }

        public static void WriteLog(string Content, string FilePrefix)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["LogPath"];
            string FileName = path + FilePrefix + ".txt";
            StringBuilder sb = new StringBuilder();
            StreamWriter sw;
            if (!File.Exists(FileName))
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                sw = File.CreateText(FileName);
                sb.Append(Content);
                sw.WriteLine(sb);
                sw.Close();
            }
            else
            {
                sw = File.AppendText(FileName);
                sb.Append(Content);
                sw.WriteLine(sb);
                sw.Close();
            }
        }

        #region Create XML for PSPVendorCall

        public string GeneratePSPRequest(string userName, string password, string driverFname, string driverLname, string DOB, string DLNum, string DLState)
        {
            StringBuilder ObjXML = new StringBuilder();

            ObjXML.Append("<soapenv:Envelope xmlns:dot='http://schemas.datacontract.org/2004/07/Dot.Psp.Common' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'> ");
            ObjXML.Append("<soapenv:Header> ");
            ObjXML.Append("<wsse:Security soapenv:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>");
            ObjXML.Append("<wsse:UsernameToken wsu:Id='UsernameToken-4' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'> ");
            ObjXML.Append("<wsse:Username>" + userName + "</wsse:Username> ");
            ObjXML.Append("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>" + password + "</wsse:Password>");
            ObjXML.Append("<wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>cur2TD5NrfjhegS6DFQnnA==</wsse:Nonce>");
            ObjXML.Append("<wsu:Created>2011-01-11T21:32:14.692Z</wsu:Created>");
            ObjXML.Append("</wsse:UsernameToken>");
            ObjXML.Append("</wsse:Security>");
            ObjXML.Append("</soapenv:Header>");
            ObjXML.Append("<soapenv:Body>");
            ObjXML.Append("<tem:GetDriverData>");
            ObjXML.Append("<tem:request>");
            ObjXML.Append("<dot:DOTNumber>0</dot:DOTNumber>");
            ObjXML.Append("<dot:UserName>" + userName + "</dot:UserName>");
            ObjXML.Append("<dot:Password> " + password + " </dot:Password>");
            ObjXML.Append("<dot:UserIPAddress/>");
            ObjXML.Append("<dot:DriverFirstName>" + driverFname + "</dot:DriverFirstName>");
            ObjXML.Append("<dot:DriverLastName> " + driverLname + " </dot:DriverLastName>");
            ObjXML.Append("<dot:DriverDOB>" + DOB + "</dot:DriverDOB>");
            ObjXML.Append("<dot:MotorCarrierId></dot:MotorCarrierId>");
            ObjXML.Append("<dot:InternalRefId> </dot:InternalRefId>");
            ObjXML.Append("<dot:DriverConsent>true</dot:DriverConsent>");
            ObjXML.Append("<dot:AuthCode xsi:nil='true' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'/>");
            ObjXML.Append("<dot:DriverQueries>");
            ObjXML.Append("<dot:DriverQuery>");
            ObjXML.Append("<dot:DlNum>" + DLNum + "</dot:DlNum>");
            ObjXML.Append("<dot:DlLastName>" + driverLname + "</dot:DlLastName>");
            ObjXML.Append("<dot:DlState>" + DLState + "</dot:DlState>");
            ObjXML.Append("</dot:DriverQuery>");
            ObjXML.Append("</dot:DriverQueries>");
            ObjXML.Append("</tem:request>");
            ObjXML.Append("</tem:GetDriverData>");
            ObjXML.Append("</soapenv:Body>");
            ObjXML.Append("</soapenv:Envelope>");
            return ObjXML.ToString();
        }

        #endregion




        //INT-116 change for PSP
        public string SendRequestforPSP(string xmlRequest)
        {
            string xmlResponse = null;
            string pageName = Convert.ToString(ConfigurationManager.AppSettings["psp_url"]);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(pageName);
            req.Method = "POST";
            req.ContentType = "text/xml;charset=UTF-8";
            req.Headers.Add("SOAPAction", "\"https://webservices.softechinternational.com/getReport\"");
            byte[] reqBytes = new UTF8Encoding().GetBytes(xmlRequest);
            req.ContentLength = reqBytes.Length;
            try
            {
                using (Stream reqStream = req.GetRequestStream())
                {
                    reqStream.Write(reqBytes, 0, reqBytes.Length);
                }
            }
            catch (Exception ex)
            {


            }
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
            {
                xmlResponse = sr.ReadToEnd();
            }
            return xmlResponse;

        }
        // INT-116

        /// <summary>
        /// To get the response from the verification service.
        /// this is newly integrated service.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public string SendRequestForFRS(string uri, string content)
        {
            string Response = "";
            // set up the uri
            Uri u = new Uri("https://orders.firstchoiceresearch.com:443/cgi-bin/pub/researcherxml");
            UriBuilder ub = new UriBuilder(u);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(ub.Uri);
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.Timeout = 65000;
            request.Method = WebRequestMethods.Http.Post;
            request.ContentLength = content.Length;
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(content);
            writer.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            Response = reader.ReadToEnd();
            response.Close();
            return Response;
        }



        public string CreateRequestForVerification(GeneralService ObjGeneralService)
        {
            string requestStr = string.Empty;
            try
            {
                string sClientID = ConfigurationManager.AppSettings["VerificationClientId"];
                string sPassword = ConfigurationManager.AppSettings["VerificationPassowrd"];
                string sClientSubID = ConfigurationManager.AppSettings["VerificationClientSubID"];
                string req = ObjGeneralService.Request;
                GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + req, "FRS/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                //Proxy class instance.
                using (RPB2BServer verificationObj = new RPB2BServer())
                {
                    //Send request to vendor.
                    requestStr = verificationObj.submitRPOrder(ref sClientID, ref sPassword, ref req, ref sClientSubID);
                    GeneralService.WriteLog(Environment.NewLine + "Request (" + DateTime.Now.ToString() + "):RunReport(GeneralService ObjGeneralService)\n" + Environment.NewLine + requestStr, "FRS/" + ObjGeneralService.OrderId + "_" + ObjGeneralService.ProductCode + "_" + ObjGeneralService.OrderDetailId);
                }

            }
            catch (Exception)
            {

                requestStr = "Error in request";
            }
            return requestStr;
        }



        /// <summary>
        /// To send the verification product request to vendor end.
        /// </summary>
        public string SendRequestForVerification(GeneralService ObjGeneralService)
        {
            string responseStr = string.Empty;
            try
            {
                string sClientID = ConfigurationManager.AppSettings["VerificationClientId"];
                string sPassword = ConfigurationManager.AppSettings["VerificationPassowrd"];
                string sClientSubID = ConfigurationManager.AppSettings["VerificationClientSubID"];
                //Proxy class instance.
                using (RPB2BServer verificationObj = new RPB2BServer())
                {
                    //Send request to vendor.
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.InnerXml = ObjGeneralService.Request;
                    //Get the Order Id.
                    string getOrderValue = xdoc.SelectSingleNode("orderConfirmation/RPOrderID").FirstChild.Value;
                    //Get the final response.
                    responseStr = verificationObj.getRPOrder(ref sClientID, ref sPassword, ref sClientSubID, ref getOrderValue);
                }
            }
            catch (Exception)
            {

                responseStr = "Error in response";
            }
            return responseStr;
        }

        /// <summary>
        /// To send the verification product request to vendor end.
        /// </summary>
        public string[] SendRequestForNonInstantMVR()//MVR_Non_Instant
        {
            string InStiUrl = string.Empty;
            string[] strArr = new string[2];
            try
            {
                string cAccountId = ConfigurationManager.AppSettings["MVRaccountId"];
                string sUserId = ConfigurationManager.AppSettings["MVRuserId"];
                string sPassword = ConfigurationManager.AppSettings["MVRpassword"];
                InStiUrl = "https://sticonnect.mvrs.com/STIConnect/StiConnectWebService.svc/ReceiveRecords";

                string lQueryString = string.Format("CUSER={0}&CCODE={1}&CPASS={2}&CUSTOMOUTPUTTYPE={3}&EMBEDDEDREPORTS={4}", sUserId, cAccountId, sPassword, 32, "");
                strArr[0] = InStiUrl;
                strArr[1] = lQueryString;

            }
            catch (Exception)
            {

                strArr[0] = "Error in production URL";
                strArr[1] = "Error in query string URL";

            }
            return strArr;
        }

        /// <summary>
        /// Http_request - Send request to host.
        /// param inMethod - HTTP Method
        /// param InStiUrl - Url to hit
        /// param inQueryStr - paameters list to send if inMethod=POST
        /// param InTimeout - Time to wait for response in miliseconds
        /// </summary>
        virtual public string HttpRequest(string inMethod, string InStiUrl, string inQueryStr, int InTimeout)
        {

            string lResponse = String.Empty;
            System.Net.WebRequest lStiRequest = System.Net.WebRequest.Create(InStiUrl);
            lStiRequest.Timeout = InTimeout;
            try
            {
                switch (inMethod)
                {
                    case "GET":
                        // Configure the web request in order to execute a HTTP GET.
                        // Just set the method property of the web request to "GET". 
                        lStiRequest.Method = "GET";
                        break;
                    case "POST":
                        {
                            // Configure the web request in order to execute a HTTP POST.
                            lStiRequest.Method = "POST";
                            //To send parameters on the body the content type of the request must to be set to application/x-www-form-urlencoded. 
                            lStiRequest.ContentType = "application/x-www-form-urlencoded";
                            //Convert the query string to the proper type, set the value of the ContentLength property of the web request.
                            byte[] lDataToSend = Encoding.UTF8.GetBytes(inQueryStr);
                            lStiRequest.ContentLength = lDataToSend.Length;
                            using (System.IO.Stream sendStream = lStiRequest.GetRequestStream())
                            {
                                // Send the parameters to the web service.
                                sendStream.Write(lDataToSend, 0, lDataToSend.Length);
                                sendStream.Close();
                            }
                        }
                        break;
                    default:
                        {
                            // Send an error response back. This function only accepts HTTP methods GET and POST.   
                            lResponse = "ERROR: " + "Invalid HTTP method.";
                            return lResponse;
                        }
                }
                // Call the function GetResponse to get the response from the web service.
                lResponse = GetResponse(lStiRequest);
            }
            catch (Exception ex)
            {
                // Catch any error thrown while configuring the web request or calling the web service.
                lResponse = "ERROR: " + ex.Message;
            }
            return lResponse;
        }/// End Http_request


        /// <summary>
        /// GetResponse - Get response string.
        /// </summary>
        virtual public string GetResponse(System.Net.WebRequest inRequest)
        {
            string result = "";
            try
            {
                // Get the response stream from the web service.
                System.Net.WebResponse lresp = inRequest.GetResponse();
                using (System.IO.Stream stream = lresp.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        //Convert the response to a string type.
                        result = sr.ReadToEnd();
                        sr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                // Catch any error thrown while getting or manipulating the response from the web service.
                result = "ERROR: " + ex.Message;
            }
            return result;
        }/// End GetResponse

    }

}
